const config = useRuntimeConfig();
const URL_Auth = config.public.URL_Auth;
const accessToken = localStorage.getItem('accessToken');

export default defineNuxtRouteMiddleware((from) => {
    console.info("PIPAD AUTH ACTIVATED");

    // หากไม่มี accessToken จะเด้งไปหน้า login
    if (!accessToken) {
        console.log("TOKEN FAILED");
        if (from.path !== '/Pipad/pipad_login') {
            return navigateTo('/Pipad/pipad_login');

        }

    }

    //ถ้ามีค่าใน userData หรือ accessToken ให้ทำการ Auth accessToken
    if (accessToken) {
        fetch(`${URL_Auth}/api/auth/user`, {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${accessToken}`,
            },
        })
            .then(response => {
                // หาก error 401  403 หรือ 404 เด้งไปหน้า login
                if (response.status === 401 || response.status === 403 || response.status === 404) {
                    console.log("LOGIN FAILED");
                    return navigateTo('/Pipad/pipad_login');
                }
                // หาก login ได้ก็จะ log ออกมาว่าสำเร็จ
                console.log("LOGIN PASS");
                if (from.path == '/Pipad/pipad_login') {
                    return navigateTo('/Pipad/resume');

                }

            })
            .catch(error => {
                // ถ้ามี error นอกเหนือที่ระบุก็จะ log error เอาไว้แล้วเด้งเข้าหน้า login
                console.error('Error fetching user data:', error);
                return navigateTo('/Pipad/pipad_login');
            });
    }
})
